# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlshwan.eu>
# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.0-or-later

from __future__ import annotations

import configparser
import copy
import datetime
import json
import logging
import os
import subprocess
from argparse import Namespace
from importlib.metadata import version

import polib
import requests
import yaml
from hugo_gettext import utils
from hugo_gettext.compilation import compile_po
from hugo_gettext.config import initialize
from hugo_gettext.generation.g_domain import HugoDomainG
from hugo_gettext.generation.g_lang import HugoLangG
from hugo_gettext.generation.index import Generation
from hugo_gettext.generation.renderer_hugo_l10n import RendererHugoL10N
from hugoi18n import customs as hg_customs
from hugoi18n.fetch import fetch_langs, revert_lang_code
from markdown_gettext.domain_generation import gettext_func


def get_snap_url(common_id):
    req_url = f'http://api.snapcraft.io/v2/snaps/find?fields=store-url&common-id={common_id}'
    headers = {'Snap-Device-Series': '16'}
    try:
        req = requests.get(req_url, headers=headers)
        url = req.json()['results'][0]['snap']['store-url']
    except (requests.exceptions.ConnectionError, ValueError) as err:
        logging.exception(f'Snapcrap! {common_id}: {err}')
        url = ''
    except IndexError:
        url = ''
    return url


def gen_id_matches():
    """
    Create or update the app-id-matches.yaml file containing the map from app ids to archlinux names and snap urls.
        Plugins and unmaintained apps are excluded. Fields that have been set are kept based on the assumption that
        once an app name (in archlinux) or url (in snap) is set, it's not changed. There are also many fields that are
        manually entered since they cannot be inferred from app ids.
    - For archlinux names: app short ids are used
    - For snap urls: app ids with and without '.desktop' are used
    It's not like archlinux names and snap urls are updated every day, so this only needs to run once in a while.
    """
    file_name = 'app-id-matches.yaml'
    other_addons = ['ffmpegthumbs', 'filestash', 'kamera', 'kdegraphics_thumbnailers',
                    'kdenetwork_filesharing', 'symmy', 'zeroconf_ioslave']
    starts = ['kdev-', 'kio_', 'okular-']
    ends = ['_plugins', 'part']
    old_matches = {}
    if os.path.isfile(file_name):
        with open(file_name) as f_r:
            old_matches = yaml.safe_load(f_r)
    new_matches = {}
    for f in appdata_files:
        app_id = os.path.splitext(f)[0]
        app_short_id = app_id.split('.', 2)[2]
        # total 286, - 27 - (2 + 3 + 13) - (2 + 4) - 7 = 228
        if (app_short_id in unmaintained or
                any(map(lambda s: app_short_id.startswith(s), starts)) or
                any(map(lambda s: app_short_id.endswith(s), ends)) or
                app_short_id in other_addons):
            continue

        print(f'{app_short_id}:')
        archlinux_done, snap_done = False, False
        new_matches[app_id] = {}
        if app_id in old_matches:
            if old_matches[app_id]['archlinux']['name']:
                new_matches[app_id]['archlinux'] = old_matches[app_id]['archlinux']
                archlinux_done = True
            if old_matches[app_id]['snap']['url']:
                new_matches[app_id]['snap'] = old_matches[app_id]['snap']
                snap_done = True
            if archlinux_done and snap_done:
                continue

        if not archlinux_done:
            archlinux_url = f'https://pkgstats.archlinux.de/api/packages/{app_short_id}'
            res = requests.get(archlinux_url)
            new_matches[app_id]['archlinux'] = {'name': app_short_id if res.json()['popularity'] else ''}
            print(f"\tarchlinux: {app_short_id if res.json()['popularity'] else ''}")
        if not snap_done:
            common_id_desktop = app_id + '.desktop'
            snap_url = get_snap_url(common_id_desktop) or get_snap_url(app_id)
            new_matches[app_id]['snap'] = {'url': snap_url}
            print(f'\tsnap: {snap_url}')
    with open(file_name, 'w') as f_w:
        yaml.dump(new_matches, f_w, default_flow_style=False, allow_unicode=True)


def fetch_flathub() -> dict:
    def condition_id(id_str: str) -> bool:
        return any(map(lambda s: id_str.startswith(s), ['im.kaidan.', 'org.kde.', 'org.rolisteam.']))

    all_apps = requests.get("https://flathub.org/api/v1/apps").json()
    kde_apps = {app['flatpakAppId']: {
        'downloads': 0,
        'first_date': '',
        'last_date': '',
        'downloads_per_day': 0,
        'popularity': 0
    } for app in all_apps if condition_id(app['flatpakAppId'])}

    today = datetime.date.today()
    year, month = today.year, today.month-1
    if month == 0:
        month = 12
        year -= 1

    stats_path = f'stats/{year}/{month:02}'
    if not os.path.isdir(stats_path):
        if subprocess.call(["wget", "--recursive", "--no-host-directories", "--no-parent", "--reject-regex", "backup",
                            "--accept", "*.json", f'https://flathub.org/{stats_path}/']) != 0:
            return kde_apps

    files = []
    for (dirpath, _, filenames) in os.walk(stats_path):
        files.extend(os.path.join(dirpath, filename) for filename in filenames)
    files.sort()

    for f in files:
        with open(f) as f_json:
            data = json.load(f_json)
            for app_id in data['refs']:
                if app_id in kde_apps:
                    if not kde_apps[app_id]['first_date']:
                        kde_apps[app_id]['first_date'] = data['date']
                        kde_apps[app_id]['last_date'] = data['date']
                    else:
                        kde_apps[app_id]['last_date'] = data['date']
                    arch_sum = sum([value[0] for value in data['refs'][app_id].values()])
                    kde_apps[app_id]['downloads'] += arch_sum

    for app_id in kde_apps:
        if kde_apps[app_id]['downloads'] == 0:
            kde_apps[app_id]['downloads_per_day'] = 0
        else:
            first_date = datetime.datetime.strptime(kde_apps[app_id]['first_date'], '%Y/%m/%d')
            last_date = datetime.datetime.strptime(kde_apps[app_id]['last_date'], '%Y/%m/%d')
            days = (last_date - first_date).days + 1
            kde_apps[app_id]['downloads_per_day'] = kde_apps[app_id]['downloads'] / days
    download_sum_per_day = sum([kde_apps[app_id]['downloads_per_day'] for app_id in kde_apps])
    for app_id in kde_apps:
        kde_apps[app_id]['popularity'] = kde_apps[app_id]['downloads_per_day'] / download_sum_per_day * 100
    return kde_apps


def fetch_archlinux_snap() -> dict:
    with open('app-id-matches.yaml') as f:
        kde_apps = yaml.safe_load(f)
        for app_id in kde_apps:
            app_name = kde_apps[app_id]['archlinux']['name']
            if app_name:
                url = f'https://pkgstats.archlinux.de/api/packages/{app_name}'
                res = requests.get(url)
                popularity = res.json()['popularity']
                # print(f'{app_name}: {popularity}')
                kde_apps[app_id]['archlinux']['popularity'] = popularity
        return kde_apps


def prepare_screenshot_data(data: dict):
    data['screenshots'] = []
    data['screenshot_captions'] = []
    data['images'] = []
    for screenshot in data.pop('Screenshots', []):
        if 'source-image' in screenshot:
            data['screenshots'].append({'url': screenshot['source-image']['url']})
            data['screenshot_captions'].append(screenshot.get('caption', {}))
            data['images'].append(screenshot['source-image']['url'])


def prepare_release_data(data: dict):
    data['releases'] = []
    data['release_descriptions'] = []
    latest_stable_release = None
    latest_stable_release_timestamp = -1
    latest_development_release = None
    latest_development_release_timestamp = -1
    platforms = set()
    for release in data.pop('Releases', []):
        if 'unix-timestamp' in release:
            release['date'] = datetime.date.fromtimestamp(int(release['unix-timestamp'])).isoformat()
        else:
            continue

        for artifact in release.get('artifacts', []):
            if artifact['type'] == 'binary':
                if artifact['platform'].startswith('win') or artifact['platform'].startswith('x86_64-windows'):
                    platforms.add('windows')
                elif artifact['platform'].startswith('android'):
                    platforms.add("android")
                elif artifact['platform'].startswith('mac') or artifact['platform'].startswith('x86_64-macos'):
                    # Hugo will make 'macOS' in frontmatter into 'macos' anyway, so we use 'macos'
                    platforms.add('macos')

        if 'url' in release:
            release['url'] = release['url']['details']
        
        if int(release['unix-timestamp']) < datetime.datetime.now().timestamp():
            if release['type'] == "stable" and release["unix-timestamp"] > latest_stable_release_timestamp:
                latest_stable_release_timestamp = release["unix-timestamp"]
                latest_stable_release = release
            elif release['type'] == "development" and release["unix-timestamp"] > latest_development_release_timestamp:
                latest_development_release_timestamp = release["unix-timestamp"]
                latest_development_release = release

            data['release_descriptions'].append(release.pop('description', {}))
            data['releases'].append(release)

    if latest_development_release:
        data['latestDevelopmentRelease'] = latest_development_release
    if latest_stable_release:
        data['latestStableRelease'] = latest_stable_release
    return platforms


def prepare_common_data(data: dict, flathub_apps, archlinux_snap):
    app_id = data['ID']
    app_short_id = app_id.split('.', 2)[2]

    data['repository'] = data.pop('X-KDE-Repository', '')
    data['appType'] = data.pop('Type')

    if app_short_id in unmaintained:
        data['Categories'] = ['unmaintained']
    elif 'Categories' not in data or not data['Categories']:
        data['Categories'] = ['addons'] if data['appType'] == 'addon' else ['unmaintained']
    # otherwise, when data['Categories'] has at least one item, keep it

    data['MainCategory'] = data['Categories'][0]
    if len(data['Categories']) > 1 and '/' in data['repository']:
        repo_category = data['repository'].split('/')[0]
        if repo_category in data['Categories']:
            data['MainCategory'] = repo_category
        elif repo_category == 'pim' and 'office' in data['Categories']:
            data['MainCategory'] = 'office'
    # now 'Categories' is a list of lower-case English names of categories, which can be considered category 'codes';
    #     localized category names are in files in 'content/categories' directory;
    #     'MainCategory' is the first element in 'Categories', i.e. the first category 'code'.

    data['flathub'] = app_id in flathub_apps
    data['snap_url'] = archlinux_snap[app_id]['snap']['url'] if app_id in archlinux_snap else ''

    archlinux_popularity = archlinux_snap[app_id]['archlinux'].get('popularity', 0) if app_id in archlinux_snap else 0
    flathub_popularity = flathub_apps[app_id]['popularity'] if app_id in flathub_apps else 0
    flathub_weight = 0.5
    data['popularity'] = flathub_popularity * flathub_weight + archlinux_popularity * (1 - flathub_weight)

    if 'Icon' in data:
        icon = data.pop('Icon')
        data['icon'] = icon['local'][0]['name']

    prepare_screenshot_data(data)
    platforms = prepare_release_data(data)

    customs = data.pop('Custom', {})
    if 'KDE::windows_store' in customs:
        data['windowsStoreLink'] = customs['KDE::windows_store']
        platforms.add("windows")
    if 'KDE::f_droid' in customs:
        data['fDroidLink'] = customs['KDE::f_droid']
        platforms.add("android")
    if 'KDE::google_play' in customs and app_short_id not in {'kstars', 'kalgebramobile'}:
        data['googlePlayLink'] = customs['KDE::google_play']
        platforms.add("android")
    if 'KDE::app_store' in customs:
        data['app_store'] = customs['KDE::app_store']
        platforms.add('ios')
    data['platforms'] = sorted(list(platforms))
    data['forum'] = customs.get('KDE::forum', '')
    data['irc'] = customs.get('KDE::irc', '')
    data['mailinglist'] = customs.get('KDE::mailinglist', '')
    data['matrixChannel'] = customs.get('KDE::matrix', '')
    data['develMailingList'] = customs.get('KDE::mailinglist-devel', '')
    data['mastodon'] = customs.get('KDE::mastodon', '')

    urls = data.pop('Url', {})
    data['homepage'] = urls.get('homepage', '')
    for s in ['www.kde.org', '://kde.org', 'invent.kde.org', 'apps.kde.org', 'games.kde.org', 'edu.kde.org']:
        if s in data['homepage']:
            data['homepage'] = ''
            break
    data['bugtracker'] = urls.get('bugtracker', '')
    data['help'] = urls.get('help', '')
    data['donation'] = urls.get('donation', '')
    data['contribute'] = urls.get('contribute', '')


def get_localized(data, lang: str) -> str:
    return data.get(lang, data.get('en', ''))


def prepare_localized_data(data, working_lang_codes):
    app_id = data['ID']
    app_short_id = app_id.split('.', 2)[2]

    name = data.pop('Name')
    summary = data.pop('Summary')
    dev_name = data.pop('DeveloperName', {})
    description = data.pop('Description', {})
    generic_name = data.pop('X-KDE-GenericName')
    screenshots = data.pop('screenshots')
    captions = data.pop('screenshot_captions')
    releases = data.pop('releases')
    release_descriptions = data.pop('release_descriptions')
    # at this point data contains only common fields
    data['localized'] = {}
    for lang in working_lang_codes:
        app_name = get_localized(name, lang)
        app_summary = get_localized(summary, lang)
        localized_props = {'name': app_name,
                           'title': app_name,
                           'summary': app_summary,
                           'description': app_summary,
                           'dev_name': get_localized(dev_name, lang),
                           'GenericName': get_localized(generic_name, lang)}
        if description:
            localized_props['appDescription'] = get_localized(description, lang)
        localized_props['aliases'] = (
                [f'/{app_id}'] +
                [a for c in data['Categories'] for a in [f'/{c}/{app_id}', f'/{c}/{app_short_id}']] if lang == 'en'
                else [f'/{lang}/{app_id}'] +
                     [a for c in data['Categories'] for a in [f'/{lang}/{c}/{app_id}', f'/{lang}/{c}/{app_short_id}']])
        localized_screenshots = copy.deepcopy(screenshots)
        for idx, s in enumerate(localized_screenshots):
            s['caption'] = get_localized(captions[idx], lang)
        localized_props['screenshots'] = localized_screenshots
        localized_releases = copy.deepcopy(releases)
        for idx, r in enumerate(localized_releases):
            r['description'] = get_localized(release_descriptions[idx], lang)
        localized_props['releases'] = localized_releases
        data['localized'][lang] = localized_props


def prepare_app_data():
    flathub_apps = fetch_flathub()
    archlinux_snap = fetch_archlinux_snap()

    app_data = []
    app_counts = {}

    for file_name in appdata_files:
        with open(f'static/appdata/{file_name}') as f:
            data = yaml.safe_load(f)
        prepare_common_data(data, flathub_apps, archlinux_snap)
        app_data.append(data)

        for lang_code in data['Summary']:
            if lang_code in app_counts:
                app_counts[lang_code] += 1
            else:
                app_counts[lang_code] = 1

    # only process languages with more than 50 translated apps
    working_lang_codes = dict(filter(lambda elem: elem[1] > 50, app_counts.items())).keys()
    for data in app_data:
        prepare_localized_data(data, working_lang_codes)
    return app_data, working_lang_codes


def write_file_frontmatter(path: str, *fms: dict):
    with open(path, 'w') as f:
        f.write('---\n')
        for fm in fms:
            f.write(yaml.dump(fm, allow_unicode=True))
        f.write('---\n')


def gen_one_app(data):
    app_short_id = data['ID'].split('.', 2)[2]
    # index.html
    if app_short_id == 'index':
        app_short_id = 'index-fm'
    localized_data = data.pop('localized', {})
    for lang, lang_data in localized_data.items():
        output_file = f'content/applications/{app_short_id}.{lang}.md'
        write_file_frontmatter(output_file, data, localized_data[lang])


def gen_term_file(taxo, term, hugo_lang_code, i18n_strings, en_strings):
    file_path = f'content/{taxo}/{term}/_index.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': i18n_strings.get(term, en_strings[term])['other'],
        'aliases': [f'{lang_prefix}/{term}']
    }
    if term in cats_with_subcats:
        content['layout'] = f'{term}_cat'
    write_file_frontmatter(file_path, content)


def gen_subcat_file(cat, subcat, hugo_lang_code, lang_subcat_name):
    file_path = f'content/categories/{cat}/{subcat}.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': lang_subcat_name,
        'aliases': [f'{lang_prefix}/{cat}/{subcat}'],
        'layout': f'{cat}_subcat'
    }
    write_file_frontmatter(file_path, content)


def gen_categories(working_lang_codes):
    # subcategory icon names have '-'
    categories = [os.path.splitext(x)[0] for x in os.listdir('static/app-icons/categories') if '-' not in x]
    # we don't need 'settingsmenu'. And 'unmaintained' is handled by 'gen_others'
    to_skip = ['settingsmenu', 'unmaintained']
    categories = [x for x in categories if x not in to_skip]

    # translate category names
    # we can also use desktop files in plasma-workspace/share/desktop-directories
    # but PO messages are grouped by languages, much more convenient to work with
    # than strings in those desktop files
    po_dir = 'pos_cat'
    os.environ['PACKAGE'] = 'plasma-workspace'
    os.environ['FILENAME'] = 'plasma-workspace._desktop_'
    fetch_langs(working_lang_codes, po_dir, as_static=False)

    for cat in categories:
        os.makedirs(f'content/categories/{cat}', exist_ok=True)

    for hugo_lang_code in working_lang_codes:
        strings = {}
        lang_code = revert_lang_code(hugo_lang_code)
        po_path = f'{po_dir}/{lang_code}.po'
        if os.path.isfile(po_path):
            po_file = polib.pofile(po_path)

            def translate(msgid: str) -> str:
                return po_file.find(msgid).msgstr
        else:
            def translate(msgid: str) -> str:
                return msgid

        for cat in categories:
            strings[cat] = {'other': translate(cat.capitalize())}
            gen_term_file('categories', cat, hugo_lang_code, strings, strings)
        for cat in cats_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                lang_subcat_name = translate(subcat_name)
                gen_subcat_file(cat, subcat_code, hugo_lang_code, lang_subcat_name)
                strings[f'{cat}-{subcat_code}'] = {'other': lang_subcat_name}
        for cat in cats_not_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                strings[f'{cat}-{subcat_code}'] = {'other': translate(subcat_name)}

        i18n_path = f'i18n/{hugo_lang_code}.yaml'
        with open(i18n_path, 'a') as f_i18n:
            f_i18n.write(yaml.dump(strings, default_flow_style=False, allow_unicode=True))
    # shutil.rmtree(po_dir)


def get_subcat_names(cat: str) -> dict[str, str]:
    if cat == 'education':
        cat = 'edu'
    subcat_desktop_files = [x for x in os.listdir(desktopDirPath) if x.startswith(f'kf5-{cat}-')]
    subcat_pairs = {}
    for subcat_desktop_file in subcat_desktop_files:
        subcat_code = subcat_desktop_file.split('.')[0].split('-')[2]
        with open(f'{desktopDirPath}/{subcat_desktop_file}') as f_desktop:
            cp = configparser.ConfigParser()
            cp.read_file(f_desktop)
            section = cp['Desktop Entry']
            subcat_pairs[subcat_code] = section['Name']
    return subcat_pairs


def gen_home_index(hugo_lang_code, tr=None):
    file_path = f'content/_index.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    title = 'KDE Applications'
    content = {
        'aliases': [f'{lang_prefix}/applications'],
        'title': tr(title) if tr else title
    }
    write_file_frontmatter(file_path, content)


def gen_section_index(section, hugo_lang_code, i18n_strings, en_strings):
    file_path = f'content/{section}/_index.{hugo_lang_code}.md'
    # section name string keys ('categories', 'platforms') need to be fixed
    title = i18n_strings.get(section, en_strings[section])['other']
    content = {
        'menu': {'main': {'weight': 2 if section == 'categories' else 3}},
        'title': title
    }
    write_file_frontmatter(file_path, content)


def gen_others(working_lang_codes):
    hg_config, mdi = initialize(RendererHugoL10N, hg_customs.__file__)
    configs = hg_config.hugo_config
    en_strings = utils.read_file(hg_config.string_file_path)
    original_configs = copy.deepcopy(configs)
    src_data = utils.read_data_files(hg_config.data)
    g = Generation(en_strings, src_data, hg_config, mdi)

    lang_config = list(configs['languages'].keys())
    for config_lang_code in lang_config:
        if config_lang_code not in working_lang_codes:
            del configs['languages'][config_lang_code]

    for platform in other_platforms:
        os.makedirs(f'content/platforms/{platform}', exist_ok=True)
    os.makedirs(f'content/categories/unmaintained', exist_ok=True)
    for hugo_lang_code in working_lang_codes:
        if hugo_lang_code in ['en']:
            tr = None
            i18n_strings = en_strings
        else:
            lang_code = revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            tr = gettext_func('apps-kde-org')

            lang_g = HugoLangG(g, lang_code)
            lang_g.default_domain_g = HugoDomainG(lang_g, tr)
            strings_result = lang_g.localize_strings()
            i18n_strings = strings_result.localized
            lang_g.write_strings(i18n_strings)
            lang_g.localize_languages()
            lang_g.localize_menu()
            lang_g.localize_description()
            lang_g.localize_title()
            lang_g.generate_data_files()

        gen_section_index('platforms', hugo_lang_code, i18n_strings, en_strings)
        for platform in other_platforms:
            gen_term_file('platforms', platform, hugo_lang_code, i18n_strings, en_strings)
        gen_section_index('categories', hugo_lang_code, i18n_strings, en_strings)
        gen_home_index(hugo_lang_code, tr)
        gen_term_file('categories', 'unmaintained', hugo_lang_code, i18n_strings, en_strings)

    if configs != original_configs:
        utils.write_file(hg_config.config_path, configs)


if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    hugoi18n_version = version('hugoi18n')
    logging.info(f'Using hugoi18n v{hugoi18n_version}')

    subprocess.run(['python3', 'extractor.py'], cwd='appdata-extractor/', check=True)

    with open('appdata-extractor/unmaintained_projects.yaml') as f_p:
        projects_w_repo, projects_wo_repo = yaml.safe_load_all(f_p)
        unmaintained_projects = projects_w_repo + projects_wo_repo
    with open('appdata-extractor/unmaintained_apps.yaml') as f_a:
        unmaintained_apps = yaml.safe_load(f_a)
    unmaintained = unmaintained_projects + unmaintained_apps

    to_ignore = ['.gitignore']
    appdata_files = set(os.listdir('static/appdata')) - set(to_ignore)

    # gen_id_matches()  # this only needs to run once in a while or only when necessary

    all_app_data, working_langs = prepare_app_data()

    os.makedirs('content/applications', exist_ok=True)
    for one_app in all_app_data:
        gen_one_app(one_app)
    logging.info('Applications done')

    other_platforms = ['android', 'ios', 'macos', 'windows']
    # create directory before we can generate index files in 'gen_others'
    os.makedirs('content/categories', exist_ok=True)
    # we need to have cats_with_subcats before it can be used in gen_term_file inside gen_others
    plasmaworkspaceDirPath = 'appdata-extractor/plasma-workspace'
    # plasmaworkspaceDirPath = '/usr'
    desktopDirPath = f'{plasmaworkspaceDirPath}/share/desktop-directories'
    # key: category code
    # value: dict of (subcategory code, subcategory name)
    cats_subcats = {}
    # categories with subcategories that we will process
    cats_with_subcats = ['education', 'games', 'utilities']
    # categories not to generate subcategory files for
    cats_not_gen_subcat_files = ['education', 'games', 'utilities']
    # categories to generate subcategory files for
    cats_gen_subcat_files = (x for x in cats_with_subcats if x not in cats_not_gen_subcat_files)

    for category in cats_with_subcats:
        subcat_names = get_subcat_names(category)
        cats_subcats[category] = subcat_names
        # gen subcat list data
        with open(f'data/{category}_subcats.yaml', 'w') as f_subcats:
            f_subcats.write(yaml.dump(list(subcat_names.keys())))

    # translate strings, title, description, and menu
    app_po_dir = 'po'
    os.environ['PACKAGE'] = 'websites-apps-kde-org'
    compile_po(Namespace(dir=app_po_dir))
    gen_others(working_langs)
    # shutil.rmtree('locale')
    # shutil.rmtree(po_dir)
    logging.info('L10n done')

    # categories and subcategories generation: this needs to be after i18n files have been generated
    gen_categories(working_langs)
    logging.info('Categories done')
