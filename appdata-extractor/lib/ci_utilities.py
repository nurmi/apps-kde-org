# SPDX-FileCopyrightText: 2021 Ben Cooksley <bcooksley@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import json
import logging
import os.path
import shutil
import tarfile
import tempfile

import gitlab


class Registry:
    def __init__(self, local_cache, gl_instance, gl_project):
        self.local_cache = local_cache
        self.cached_packages = []
        self.remote_packages = []
        if not os.path.isdir(self.local_cache):
            os.makedirs(self.local_cache)

        # Determine what we have locally first
        cache_contents = os.listdir(self.local_cache)
        for filename in cache_contents:
            if not filename.endswith('.json'):
                continue
            full_path = os.path.join(self.local_cache, filename)
            with open(full_path) as f_json:
                package_metadata = json.load(f_json)
            self.cached_packages.append(package_metadata)

        # Now we reach out to the remote registry...
        server = gitlab.Gitlab(gl_instance)
        self.project = server.projects.get(gl_project)
        for package in self.project.packages.list(iterator=True):
            branch, ts = package.version.rsplit('-', 1)
            package_metadata = {'identifier': package.name,
                                'version': package.version,
                                'branch': branch,
                                'timestamp': int(ts)}
            self.remote_packages.append(package_metadata)

    def retrieve(self, identifier, branch):
        """
        Retrieve a package matching the supplied parameters
        :param identifier: package name
        :param branch: package branch
        :return: a tuple containing a handle to the package archive and a dict surrounding the package
        """
        remote_package = None
        cached_package = None

        for entry in self.remote_packages:
            # We have to use the normalized branch name when doing the comparison,
            #   as the entries returned from GitLab's API will be normalized
            if entry['identifier'] != identifier or entry['branch'] != branch:
                continue
            if remote_package is None:
                remote_package = entry
            if entry['timestamp'] > remote_package['timestamp']:
                remote_package = entry

        # Before we continue, did we find something?
        # If we found nothing, bow out gracefully here...
        if remote_package is None:
            return None, None

        package_name = f'{identifier}-{branch}'
        local_contents_path = os.path.join(self.local_cache, package_name + '.tar')
        local_metadata_path = os.path.join(self.local_cache, package_name + '.json')

        for entry in self.cached_packages:
            # By definition, the package cannot be newer as we have the latest remote version.
            #   If it is then something is seriously wrong
            if entry['identifier'] == identifier and entry['branch'] == branch and \
                    entry['timestamp'] == remote_package['timestamp']:
                cached_package = entry
                break
        if cached_package:
            return local_contents_path, cached_package

        # Download the archive first...
        response = self.project.generic_packages.download(package_name=remote_package['identifier'],
                                                          package_version=remote_package['version'],
                                                          file_name='archive.tar')
        latest_content = tempfile.NamedTemporaryFile(delete=False, mode='wb', dir=self.local_cache)
        latest_content.write(response)
        latest_content.close()

        # Now the metadata file...
        response = self.project.generic_packages.download(package_name=remote_package['identifier'],
                                                          package_version=remote_package['version'],
                                                          file_name='metadata.json')
        latest_metadata = tempfile.NamedTemporaryFile(delete=False, mode='wb', dir=self.local_cache)
        latest_metadata.write(response)
        latest_metadata.close()

        # Move both to the cache for future use
        shutil.move(latest_content.name, local_contents_path)
        shutil.move(latest_metadata.name, local_metadata_path)

        return local_contents_path, json.load(open(local_metadata_path))

    def retrieve_install(self, identifier: str, destination: str, branch: str):
        logging.info(f'Retrieving {branch} branch of {identifier}...')
        local_contents_path, _ = self.retrieve(identifier, branch)
        logging.info(f'Retrieved {branch} branch of {identifier}')
        archive = tarfile.open(local_contents_path, mode='r')
        archive.extractall(destination)
        logging.info(f'Extracted {identifier} to {destination}')
